#
# DATABASE Structure for `ganadera`
#
CREATE DATABASE IF NOT EXISTS `ganadera`;
USE `ganadera`;

#
# Table structure for table `Propietario`
#
CREATE TABLE `Propietario`(
    `id_Personal` INT  NOT NULL AUTO_INCREMENT,
     `id_Finca` INT  NOT NULL,
    `Clave` VARCHAR (8),
    `Nombre` VARCHAR (25),
    `Apellido` VARCHAR (25),
    `Correo` VARCHAR (40),
    'Telefono' VARCHAR (15),
    `Fecha_Registro` DATE,
    PRIMARY KEY (`id_Personal`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Finca`
#
CREATE TABLE `Finca`(
    `id_Finca` INT  NOT NULL AUTO_INCREMENT,
     `id_Propietario` INT  NOT NULL,
    `Nombre` VARCHAR(25),
    `Explotacion_Tipo` VARCHAR(20),
    PRIMARY KEY (`id_Finca`),
    FOREIGN KEY (`id_Propietario`) REFERENCES `Propietario` (`id_Personal`)
);

#
# Table structure for table `Hierro`
#
CREATE TABLE `Hierro`(
    `id_Hierro` INT  NOT NULL AUTO_INCREMENT,
     `id_Finca` INT  NOT NULL,
     `id_Personal` INT  NOT NULL,
    `Hierro_Imagen` BLOB,
    `Hierro_QR` BLOB,
    PRIMARY KEY (`id_Hierro`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
    FOREIGN KEY (`id_Personal`) REFERENCES `Propietario` (`id_Personal`),
);

#
# Table structure for table `Rebaño`
#
CREATE TABLE `Rebano`(
    `id_Rebano` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Nombre` VARCHAR(25),
    PRIMARY KEY (`id_Rebano`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Animal`
#
CREATE TABLE `Animal`(
    `id_Animal` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `id_Rebano` INT  NOT NULL,
    `Nombre` VARCHAR(25),
    `Sexo` CHAR,
    `Edad` INT,
    `Tipo` VARCHAR(20),
    `Raza` VARCHAR (20),
    `Estado` VARCHAR(20),
    `Procedencia` VARCHAR(50),
    PRIMARY KEY (`id_Animal`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`),
    FOREIGN KEY (`id_Rebano`) REFERENCES `Rebano` (`id_Rebano`)
);

#
# Table structure for table `Personal_Finca`
#
CREATE TABLE `Personal_Finca`(
    `id_Tecnico` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Nombre` VARCHAR(25),
    `Apellido` VARCHAR(25),
    `Telefono` VARCHAR(15),
    `Correo` VARCHAR(40),
    `Tipo_Trabajador` VARCHAR(20),
    PRIMARY KEY (`id_Tecnico`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Transcriptor`
#
CREATE TABLE `Transcriptor`(
    `id_Personal` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Usuario` VARCHAR (20),
    `Clave` VARCHAR (8),
    `Tipo_Transcriptor` VARCHAR (9),
    `Nombre` VARCHAR(25),
    `Apellido` VARCHAR(25),
    `Telefono` VARCHAR(15),
    `Fecha_Registro` DATE,
    PRIMARY KEY (`id_Personal`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Inventario_Búfalo`
#
CREATE TABLE `Inventario_Bufalo`(
    `id_Inv_B` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Num_Becerro` INT,
    `Num_Anojo` INT,
    `Num_Bubilla` INT,
    `Num_Bufalo` INT,
    `Fecha_Inventario` DATE,
    PRIMARY KEY (`id_Inv_B`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Inventario_Vacuno`
#
CREATE TABLE `Inventario_Vacuno`(
    `id_Inv_V` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Num_Becerra` INT,
    `Num_Mauta` INT,
    `Num_Novilla` INT,
    `Num_Vaca` INT,
    `Num_Becerro` INT,
    `Num_Maute` INT,
    `Num_Torete` INT,
    `Num_Toro` INT,
    `Fecha_Inventario` DATE,
    PRIMARY KEY (`id_Inv_V`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Inventario_General`
#
CREATE TABLE `Inventario_General`(
    `id_Inv` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Num_Personal` INT,
    `Fecha_Inventario` DATE,
    PRIMARY KEY (`id_Inv`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Terreno`
#
CREATE TABLE `Terreno`(
    `id_Terreno` INT  NOT NULL AUTO_INCREMENT,
    `id_Finca` INT  NOT NULL,
    `Superficie` FLOAT NOT NULL,
    `Relieve` VARCHAR (9),
    `Suelo_Textura` VARCHAR (25),
    `ph_Suelo` CHAR (2),
    `Precipitacion` FLOAT NOT NULL,
    `Velocidad_Viento` FLOAT NOT NULL,
    `Temp_Anual` VARCHAR (4),
    `Temp_Min` VARCHAR (4),
    `Temp_Max` VARCHAR (4),
    `Radiacion` FLOAT NOT NULL,
    `Fuente_Agua` VARCHAR (25),
    `Caudal_Disponible` INT NOT NULL,
    `Riesgo_Metodo` VARCHAR (18),
     PRIMARY KEY (`id_Terreno`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);     

#
# Table structure for table `Raza`
#
CREATE TABLE `Raza`(
    `id_Raza` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `Grupo_Raza` VARCHAR (12),
    `Nombre` VARCHAR (25),
    `Siglas` VARCHAR (6),
    `Procedencia` VARCHAR (40) ,
    PRIMARY KEY (`id_Raza`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);
 
#
# Table structure for table `Arbol_Genetica`
#
CREATE TABLE `Arbol_Genetica`(
    `id_Gen` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `Cod_pa` INT NOT NULL,
    `Cod_Abo` INT NOT NULL,
    `Cod_bisaAbo_p` INT NOT NULL,
    `Cod_bisaAbo_p` INT NOT NULL,
    `Cod_aba_P` INT NOT NULL,
    `Cod_bisaAba_p` INT NOT NULL,
    `Cod_bisaAba_p` INT NOT NULL,
    `Cod_ma` INT NOT NULL,
    `Cod_abo_M` INT NOT NULL,
    `Cod_bisaAbo_m` INT NOT NULL,
    `Cod_bisaAbo_m` INT NOT NULL,
    `Cod_aba_M` INT NOT NULL,
    `Cod_bisaAba_m` INT NOT NULL,
    `Cod_bisaAba_m` INT NOT NULL,
    PRIMARY KEY (`id_Gen`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Cambios_Animal`
#
CREATE TABLE `Cambios_Animal`(
    `id_Cambio` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `Fecha_Cambio` DATE,
    `Tipo_Cambio` VARCHAR (8),
    `Peso` FLOAT NOT NULL,
    `Altura` FLOAT NOT NULL,
    `Comentario` VARCHAR (60),
    PRIMARY KEY (`id_Cambio`), 
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Historico_Cambio`
#
CREATE TABLE `Historico_Cambio`(
    `id_Historico` INT NOT NULL AUTO_INCREMENT,
    `id_Cambio` INT NOT NULL ,
    `id_Animal` INT NOT NULL ,
    `Fecha_Cambio` DATE,
    `Tipo_Cambio` VARCHAR (8),
    `Peso` FLOAT NOT NULL,
    `Altura` FLOAT NOT NULL,
    `Comentario` VARCHAR (60),
    'Fecha_Actualizacion' DATE,
    PRIMARY KEY (`id_Historico`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`),
    FOREIGN KEY (`id_Cambio`) REFERENCES `Cambios_Animal` (`id_Cambio`)
);

#
# Table structure for table `Medidas_Corporales`
#
CREATE TABLE `Medidas_Corporales`(
    `id_Medida` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `Altura_HC` FLOAT,
    `Altura_HG` FLOAT,
    `Perimetro_PT` FLOAT,
    `Perimetro_PCA` FLOAT,
    `Longitud_LC` FLOAT,
    `Longitud_LG` FLOAT,
    `Anchura_AG` FLOAT,
    PRIMARY KEY (`id_Medida`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Historico_MedidasCor`
#
CREATE TABLE `Historico_MedidasCor`(
    `id_Historico` INT NOT NULL AUTO_INCREMENT,
    `id_Medida` INT NOT NULL ,
    `id_Animal` INT NOT NULL ,
    `Altura_HC` FLOAT,
    `Altura_HG` FLOAT,
    `Perimetro_PT` FLOAT,
    `Perimetro_PCA` FLOAT,
    `Longitud_LC` FLOAT,
    `Longitud_LG` FLOAT,
    `Anchura_AG` FLOAT,
    `Fecha_Actualizacion` DATE,
    PRIMARY KEY (`id_Medida`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`),
    FOREIGN KEY (`id_Medida`) REFERENCES `Medidas_Corporales` (`id_Medida`)
);

#
# Table structure for table `Indices_Corporales`
#
CREATE TABLE `Indices_Corporales`(
    `id_Indice` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `Anamorfosis` FLOAT,
    `Corporal` FLOAT,
    `Pelviano` FLOAT,
    `Proporcionalidad` FLOAT,
    `Dactilo_Toracico` FLOAT,
    `Pelviano_Transversal` FLOAT,
    `Pelviano_Longitudinal` FLOAT,
    PRIMARY KEY (`id_Indice`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Historico_IndicesCor`
#
CREATE TABLE `Historico_IndicesCor`(
    `id_Historico` INT NOT NULL AUTO_INCREMENT,
    `id_Indice` INT NOT NULL ,
    `id_Animal` INT NOT NULL ,
    `Anamorfosis` FLOAT,
    `Corporal` FLOAT,
    `Pelviano` FLOAT,
    `Proporcionalidad` FLOAT,
    `Dactilo_Toracico` FLOAT,
    `Pelviano_Transversal` FLOAT,
    `Pelviano_Longitudinal` FLOAT,
    `Fecha_Actualizacion` DATE,
    PRIMARY KEY (`id_Historico`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`),
    FOREIGN KEY (`id_Indice`) REFERENCES `Indices_Corporales` (`id_Indice`)
);

#
# Table structure for table `PesoCorporal`
#
CREATE TABLE `Peso_Corporal`(
    `id_Peso`  INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `id_Tecnico` INT NOT NULL ,
    `Fecha_Peso` DATE,
    `Peso` FLOAT,
    `Comentario` VARCHAR (40),
    PRIMARY KEY (`id_Historico`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`),
    FOREIGN KEY (`id_Tecnico`) REFERENCES `Personal_Finca` (`id_Tecnico`)
);

#
# Table structure for table `Registro_PesoCor`
#
CREATE TABLE `Registro_PesoCor`(
    `id_Registro`  INT NOT NULL AUTO_INCREMENT,
    `id_Peso` INT NOT NULL ,
    `id_Animal` INT NOT NULL ,
    `id_Tecnico` INT NOT NULL ,
    `Peso_Nacer` FLOAT,
    `Fecha_Nacer` DATE,
    `Peso_Destete` FLOAT,
    `Fecha_Destete` DATE,
    `Peso_Actual` FLOAT,
    `Comentario` VARCHAR (40),      
    PRIMARY KEY (`id_Registro`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`),
    FOREIGN KEY (`id_Tecnico`) REFERENCES `Personal_Finca` (`id_Tecnico`),
    FOREIGN KEY (`id_Peso`) REFERENCES `Peso_Corporal` (`id_Peso`)
);

#
# Table structure for table `Movimiento_Rebano`
#
CREATE TABLE `Movimiento_Rebano`(
    `id_Movimiento` INT NOT NULL AUTO_INCREMENT ,
    `id_Finca` INT NOT NULL ,
    `id_Rebano` INT NOT NULL ,
    `id_Animal` INT NOT NULL ,
    `Rebano_Destino` VARCHAR (30),     
    `id_Finca_Destino` INT NOT NULL,
    `Comentario` VARCHAR (40),      
    PRIMARY KEY (`id_Movimiento`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`),
    FOREIGN KEY (`id_Rebano`) REFERENCES `Rebano` (`id_Rebano`),
    FOREIGN KEY (`id_Finca`) REFERENCES `Finca` (`id_Finca`)
);

#
# Table structure for table `Reproduccion_Animal`
#
CREATE TABLE `Reproduccion_Animal`(
    `id_ReproduccionA`  INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL ,
    `Fecha_Parto` DATE TIME,
    `Fecha_Aborto` DATE TIME,
    `Tipo_Inse` VARCHAR (10),
    `Resultado_Reproduccion` VARCHAR(40),
    PRIMARY KEY (`id_Reproduccion`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Historico_ReproduccionA`
#
CREATE TABLE `Historico_ReproduccionA`(
    `id_HistorialRA`   INT NOT NULL AUTO_INCREMENT,
    `id_ReproduccionA`  INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL,
    `Fecha_Parto_Ultimo` DATE TIME,
    `Fecha_Aborto_Ultimo` DATE TIME,
    `Tipo_Inse_Ultimo` VARCHAR (10),
    `Resultado_ReproduccionUlt` VARCHAR(40),
    PRIMARY KEY (`id_HistorialRA`),
    FOREIGN KEY (`id_ReproduccionA`) REFERENCES `Reproduccion_Animal` (`id_ReproduccionA`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Leche`
#
CREATE TABLE `Leche`(
    `id_Pesaje` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL,
    `Pesaje_Fecha` DATE,
    `Pesaje_Total` FLOAT,
    PRIMARY KEY (`id_Pesaje`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Historico_Leche`
#
CREATE TABLE `Historico_Leche`(
    `id_Historico` INT NOT NULL AUTO_INCREMENT,
    `id_Pesaje` INT NOT NULL,
    `id_Animal` INT NOT NULL,
    `Fecha_Comienzo_Pesaje` DATE,
    `Fecha_Ultimo_Pesaje` DATE,
    `Pesaje_Ultimo` FLOAT,
    `Pesaje_Total` FLOAT,
    PRIMARY KEY (`id_Historico`),
    FOREIGN KEY (`id_Pesaje`) REFERENCES `Leche` (`id_Pesaje`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Animal`
# 
CREATE TABLE `Lactancia`(
    `id_Lactancia` INT NOT NULL AUTO_INCREMENT,
    `id_Animal` INT NOT NULL,
    `Lactancia_Inicio` DATE,
    `Lactancia_Fin` DATE,
    `Lactancia_Días` INT,
    `Lactancia_Peso` FLOAT,
    `Fecha_Secado` DATE,
    PRIMARY KEY (`id_Lactancia`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);

#
# Table structure for table `Historico_Lactancia`
#
CREATE TABLE `Historico_Lactancia`(
    `id_Historico` INT NOT NULL AUTO_INCREMENT,
    `id_Lactancia` INT NOT NULL,
    `id_Animal` INT NOT NULL,
    `Lactancia_Inicio` DATE,
    `Lactancia_Fin` DATE,
    `Lactancia_Días` INT,
    `Lactancia_Peso_Total` FLOAT,
    `Lactancia_Temporal` FLOAT,
    `Fecha_Secado` DATE,
    `Fecha_Actualizacion` DATE,
    PRIMARY KEY (`id_Historico`),
    FOREIGN KEY (`id_Lactancia`) REFERENCES `Lactancia` (`id_Lactancia`),
    FOREIGN KEY (`id_Animal`) REFERENCES `Animal` (`id_Animal`)
);